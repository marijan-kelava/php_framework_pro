<?php

namespace MarijanKelava\Framework\Http;

use FastRoute\RouteCollector;

use function FastRoute\SimpleDispatcher;

class Kernel
{
    public function handle(Request $request)
    {
        //$content = '<h1>Hello World</h1>';

        //return new Response($content, $status = 200, $headers = []);

        //Create a dispatcher

        $dispatcher = simpleDispatcher(function(RouteCollector $routeCollector) {

            $routes = include BASE_PATH . '/routes/web.php';

            foreach ($routes as $route) {
                $routeCollector->addRoute(...$route);
            }
        });

        /*$dispatcher = simpleDispatcher(function(RouteCollector $routeCollector) {

            $routeCollector->addRoute('GET', '/', function() {
                $content = '<h1>Hello World</h1>';

                return new Response($content, $status = 200, $headers = []);
            });

            $routeCollector->addRoute('GET', '/posts/{id:\d+}', function($routeParams) {
                $content = "<h1>This is Post {$routeParams['id']} </h1>";

                return new Response($content, $status = 200, $headers = []);
            });
        });*/
        
        //Dispatch a URI, to obtain the route info
        $routeInfo = $dispatcher->dispatch(
            $request->getMethod(),
            //$request->server['REQUEST_METHOD'],
            $request->getPathInfo(),
            //$request->server['REQUEST_URI']
        );
        
        //[$status, $handler, $vars] = $routeInfo;
        [$status, [$controller, $method], $vars] = $routeInfo;

        //Call the handler provided by the route info, in order to create response

        //return $handler($vars);
        
        /*$response = (new $controller())->$method($vars);

        return $response;*/
        dd(new $controller);
        $response = call_user_func_array([new $controller, $method], $vars);

        return $response;
    }
}