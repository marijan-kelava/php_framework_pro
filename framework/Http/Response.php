<?php

namespace MarijanKelava\Framework\Http;

class Response
{
    private string $content;
    private int $status;
    private array $headers;

    public function __construct($content, $status, $headers)
    {
        $this->content = $content;
        $this->status = $status;
        $this->headers = $headers;
    }

    public function send(): void
    {
        echo $this->content;
    }
}