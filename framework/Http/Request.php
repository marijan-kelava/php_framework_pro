<?php

namespace MarijanKelava\Framework\Http;

class Request
{
    public array $getParams;
    public array $postParams;
    public array $cookies;
    public array $files;
    public array $server;

    public function __construct($getParams, $postParams, $cookies, $files, $server)
    {
        $this->getParams = $getParams;
        $this->postParams = $postParams;
        $this->cookies = $cookies;
        $this->files = $files;
        $this->server = $server;
    }

    public static function createFromGlobals()
    {
        return new static($_GET, $_POST, $_COOKIE, $_FILES, $_SERVER);
    }

    public function getPathInfo(): string
    {
        return strtok($this->server['REQUEST_URI'], '?');
    }

    public function getMethod(): string
    {
        return $this->server['REQUEST_METHOD'];
    }

}