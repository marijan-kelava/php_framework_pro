# Php Develop

## Installation

Checkout `master` branch

## Docker Setup

Run `docker-compose up -d` to build up the containers

After that, login to `mvc_web` container `docker exec -it mvc_web bash` (var/www), and run `./setup.sh` or `composer install` 

setup.sh needs to be executable

Go to: `http://localhost:8888`  

Login to adminer use container name as server, 


