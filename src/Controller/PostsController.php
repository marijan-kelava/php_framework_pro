<?php

namespace App\Controller;

use MarijanKelava\Framework\Http\Response;

class PostsController
{
    public function show(int $id): Response
    {
        $content = "<h1>Hello Posts $id</h1>";

        return new Response($content, $status = 200, $headers = []);
    }
}